import { GET_TODOS, GET_TODO, DELETE_TODO, TOGGLE_COMPLETE_TODO } from '../action/todoActions';

const initialState = {
    todos: [],
    todo: {}
}

export default function (state = initialState, action) {
    switch (action.type) {
        case GET_TODOS:
            return {
                ...state,
                todos: action.payload
            }
        case GET_TODO:
            return {
                ...state,
                todo: action.payload
            }
        case DELETE_TODO:
            return {
                ...state,
                todos: state.todos.filter(todo => todo.id !== action.payload)
            }
        case TOGGLE_COMPLETE_TODO:
            return {
                ...state,
                todos: state.todos.map(todo => {
                    if (todo.id === action.payload) {
                        return {
                            ...todo,
                            completed: !todo.completed
                        }
                    } else {
                        return todo;
                    }
                })
            }
        default:
            return state;
    }
}