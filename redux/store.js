import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import rootReducer from '../redux/reducers'
const middleware = [thunk]
const initialState = {}

const store = (state = initialState, options) => {
    return createStore(
        rootReducer,
        state,
        compose(
            applyMiddleware(...middleware),
        )
    );
};

export default store