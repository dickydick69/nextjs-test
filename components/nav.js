import React from 'react'
import { Navbar, Nav } from 'react-bootstrap'
import Link from 'next/link'

const Menus = [
  {
    'href': '/',
    'name': 'Home'
  },
  {
    'href': '/todos',
    'name': 'Todos'
  },
  {
    'href' : '/contact_us',
    'name' : 'Contact Us'
  }
]

const nav = () => (
  <Navbar bg="dark" variant="dark">
    <Navbar.Brand>WOT?</Navbar.Brand>
    <Nav className="mr-auto">
      {
        Menus.map(menu => (
          <Link key={menu.href} href={menu.href} activeClassName="active">
            <a className="nav-link">
              {menu.name}
          </a>
          </Link>
        ))
      }
    </Nav>
  </Navbar>
)

export default nav
