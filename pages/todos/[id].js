import React from 'react'
import { connect } from 'react-redux'
import Head from 'next/head'
import Layout from '../../components/Layout'
import { getTodo } from '../../redux/action/todoActions'

function todo(props) {
    const { title, completed } = props.todo;
    return (
        <Layout>
            <Head>
                <title>{title}</title>
            </Head>
            <h1 className="display-4">
                {title}
            </h1>
        </Layout>
    )
}

todo.getInitialProps = async function ({ query, store }) {
    const { id } = query;
    await store.dispatch(getTodo(id));
}

const mapStateToProps = state => ({
    todo: state.todo.todo
})

export default connect(mapStateToProps, {})(todo)