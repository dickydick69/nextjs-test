import React from 'react'
import Link from 'next/link'
import Head from 'next/head'
import Layout from '../components/Layout'

const Home = () => {
  return (
    <Layout>
      <Head>
        <title>Home</title>
      </Head>
      <h1>NEXT HOME</h1>
    </Layout>
  );
}

export default Home
