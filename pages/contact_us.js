import React from 'react'
import Head from 'next/head'
import Layout from '../components/Layout'
import ContactForm from '../components/Contacts/ContactForm'

const ContactUs = props => {
    return (
        <Layout>
            <Head>
                <title>Contact Us</title>
            </Head>
            <ContactForm />
        </Layout>
    )
}

export default ContactUs;