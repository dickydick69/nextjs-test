import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import Head from 'next/head'
import { getTodos } from '../redux/action/todoActions'
import Layout from '../components/Layout'
import Todos from '../components/Todos'
import store from '../redux/store'

const todos = props => {
    return (
        <Layout>
            <Head>
                <title>Todos</title>
            </Head>
            <Todos todos={props.todos} />
        </Layout>
    )
}

todos.getInitialProps = async function ({ store }) {
    await store.dispatch(getTodos());
}

const mapStateToProps = state => ({
    todos: state.todo.todos
})

export default connect(mapStateToProps, { getTodos })(todos)